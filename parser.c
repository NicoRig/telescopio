/*
 * parser.c
 *
 *  Created on: Jun 20, 2016
 *      Author: nicolas
 */

#include "parser.h"


const char *comando[]={ //comandos, Debe estar ordenado en forma ascendente
	"B", //Goto AZM-ALT “B12AB,4000” 0
	"E", //Get RA/DEC  1
	"H",//Set Time 2
	"J",//Is aligned? 3
	"K",//Echo 4
	"L",//IS goto in progress? 5
	"M",// Cancel GOTO 6
	"P\x2\x10\x24", //Fixed rate Azm (or RA) slew in positive direction 7
	"P\x2\x10\x25",	//Fixed rate Azm (or RA) slew in negative direction 8
	"P\x2\x11\x24",//Fixed rate Alt (or Dec) slew in positive direction 9
	"P\x2\x11\x25",//Fixed rate Alt (or Dec) slew in negative direction 10
	"P\x3\x10\x6",// Variable rate Azm (or RA) slew in positive direction 11
	"P\x3\x10\x7", // Variable rate Azm (or RA) slew in negative direction 12
	"P\x3\x11\x6", //Variable rate Alt (or Dec) slew in positive direction 13
	"P\x3\x11\x7",//Variable rate Alt (or Dec) slew in negative direction 14
	"R", //Goto RA/DEC “r34AB,12CE” 15
	"S", //Sync Sxxxx,xxxx 16
	"T", //Set Tracking Mode 17
	"V",//Get Version 18
	"W",//Set Location 19
	"Z", //Get AZM-ALT 20
	"b", //Goto precise AZM-ALT “b12AB0500,40000500” 21
	"e", //Get Precise RA/DEC  22
	"h",//Get Time 23
	"r", //Goto Precise RA/DEC “r34AB0500,12CE0500” 24
	"s", //Sync Precise sxxxxxxxx,xxxxxxxx 25
	"t", //Get Tracking Mode 26
	"w",//Get Location 27
	"z", //Get Precise AZM-ALT 28
};
size_t cantcomandos = sizeof(comando) / sizeof(char *);

/**
 * int quecomandoes(char * cadena)
 * devuelve el numero de comando si cadena representa una cabecera
 * de comando en el protocolo Nexstar o cantcomandos + 1, 30 al momento
 * si estamos en la mitad de una cabecera de comando multicaracter
 * returns int: numero de comando o indicador de mitad de comando
 */
int quecomandoes(char * cadena){
	int min = 0,max,mid,valadevolver;
   	max = cantcomandos - 1;
   	mid = (min+max)/2;

   	while (min <= max) {
      if (strcmp(cadena,comando[mid])>0)
         min = mid + 1;
      else if (!strcmp(cadena,comando[mid])) {
         break;
      } else {
      	max = mid - 1;
      }

      mid = (min+max)/2;
   }
   valadevolver=mid;
   if (min>max) {
	   valadevolver=-1;
	   for(min = 0;min<cantcomandos;min++){ //estamos en realidad en la mitad de un comando multicaracter?
		   if(strstr(comando[min],cadena) && cadena[0]!=0){
			   valadevolver=cantcomandos+1;
			   break;
		   }
	   }
   }
	return valadevolver;
}



/*
int cstring_cmp(const void *a, const void *b)
{

    const char *ia = (const char *)a;
    const char **ib = (const char **)b;
	printf("%s,%s\n", ia,*ib);
    return strcmp(ia, *ib);
	 strcmp functions works exactly as expected from
	comparison function
}*/
