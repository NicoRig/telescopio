/*
 * motores.h
 *
 *  Created on: Jun 23, 2016
 *      Author: renzo
 */

#ifndef MOTORES_H_
#define MOTORES_H_

#include "inc/hw_ints.h"
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h" //Defines and macros for NVIC Controller (Interrupt) API of driverLib. This includes API functions such as IntEnable and IntPrioritySet.
#include "driverlib/gpio.h"
#include "driverlib/timer.h" //: Defines and macros for Timer API of driverLib. This includes API functions such as TimerConfigure and TimerLoadSet.

void Goto(uint32_t RAd,uint32_t DECd);
void TrackRA(float velocidad, int8_t sentido);
void TrackDEC(float velocidad, int8_t sentido);
void StopTrackRA(void);
void StopTrackDEC(void);
void initmotores(void);

#endif /* MOTORES_H_ */
