/*
 * motores.c
 *
 *  Created on: Jun 23, 2016
 *      Author: Renzo
 */



#include "motores.h"

extern uint32_t RA;
extern uint32_t DEC;
extern uint32_t cuentafinalRA;
extern uint32_t cuentafinalDEC;
extern uint32_t cuentaactualRA;
extern uint32_t cuentaactualDEC;


void initmotores(void){


	//SysCtlClockSet(SYSCTL_SYSDIV_5|SYSCTL_USE_PLL|SYSCTL_XTAL_16MHZ|SYSCTL_OSC_MAIN);// la frecuencia de bus sera de 40 megaherz.

	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);// Habilito puerto C
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);// Habilito puerto D
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);// Habilito puerto E
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);// Habilito puerto A

	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);// Habilito puerto F
	GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);// PORTF(1), PORTF(2), PORTF(3) seran salidas.

	GPIOPinTypeGPIOOutput(GPIO_PORTC_BASE, GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7);
	// 4,5,6,7 -->>ENABLE, MICROSTEPPING1, MICROSTEPPING2, MICROSTEPPING 3, CLOCK PARA AZIMUTAL

	GPIOPinTypeGPIOOutput(GPIO_PORTD_BASE, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);
	// 0,1,2,3 -->>ENABLE, MICROSTEPPING1, MICROSTEPPING2, MICROSTEPPING 3, CLOCK PARA ASCENCION RECTA

	GPIOPinTypeGPIOOutput(GPIO_PORTE_BASE, GPIO_PIN_1|GPIO_PIN_2);//CLOCK ASCENCION RECTA Y SENTIDO


	GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, GPIO_PIN_2|GPIO_PIN_3);//CLOCK AZIMUTAL Y SENTIDO

	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);// Habilito el uso del periferico timer0, PARA ASCENCION RECTA
	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);// Habilito el uso del periferico timer1, PARA AZIMUTAL


	TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC);// EL timer0 se configura para operar en 32bits (que consta de dos timers de 16 bits: timer0A y timer0B.)
													// El timer0 contara en forma periodica.
	TimerConfigure(TIMER1_BASE, TIMER_CFG_PERIODIC);// EL timer1 se configura para operar en 32bits (que consta de dos timers de 16 bits: timer1A y timer1B.)
													// El timer1 contara en forma periodica.

	IntMasterEnable();// Habilitacion general de interrupciones
/*
	TrackRA(0.5,1);

	SysCtlDelay(40000000);



	TrackDEC(0.5,1);

		SysCtlDelay(40000000);

		StopTrackRA();
		StopTrackDEC();

	while(1)
	{


	}*/
}

/**
 * TODO
 */
void Goto(uint32_t RAd,uint32_t DECd){
	int32_t deltaRA = RAd - RA;
	int32_t deltaDEC = DECd - DEC;
	uint8_t signodeltaRA = 1;
	uint8_t signodeltaDEC= 1;
	if(deltaRA<0){
		deltaRA *= -1;
		signodeltaRA=0;
	}
	if(deltaDEC<0){
		deltaDEC *= -1;
		signodeltaDEC=0;
	}
	cuentafinalRA=deltaRA/49710;
	cuentafinalDEC=deltaDEC/49710;

	if(cuentafinalRA){
		TimerLoadSet(TIMER0_BASE, TIMER_A,1247021/20); //cargo el valor del semiperiodo en el timer RA
		IntEnable(INT_TIMER0A);// Se habilitan las interrupciones por timer0A
		TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);//Las interrupciones del timer0A se daran cuanto se alcanza la cuenta maxima,
		GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_2, 0); //ARRANCO EN CERO LA SEÑAL DE CLOCK
		GPIOPinWrite(GPIO_PORTC_BASE, GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7, 0 );
		//PIN 4 EN BAJO(ENABLE HABILITADO), PIN 5 , PIN 6, PIN 7 EN BAJO (FULL)
		if(!signodeltaRA){
			GPIOPinWrite(GPIO_PORTA_BASE,GPIO_PIN_3, 0 );//SENTIDO POSITIVO
		} else {
			GPIOPinWrite(GPIO_PORTA_BASE,GPIO_PIN_3, GPIO_PIN_3 );//SENTIDO NEGATIVO
		}
		TimerEnable(TIMER0_BASE, TIMER_A);// Habilito la operacion del timer: comienza la cuenta
	}

	if(cuentafinalDEC){ //Efectivamente debo moverme
		//MOTOR DECLINACION
		TimerLoadSet(TIMER1_BASE, TIMER_A,1247021/20); //cargo el valor del semiperiodo en el timer
		IntEnable(INT_TIMER1A);// Se habilitan las interrupciones por timer0A
		TimerIntEnable(TIMER1_BASE, TIMER_TIMA_TIMEOUT);//Las interrupciones del timer0A se daran cuanto se alcanza la cuenta maxima,
		GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1, 0); //ARRANCO EN CERO LA SEÑAL DE CLOCK
		GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3,0 );
		//PIN 0 EN BAJO(ENABLE HABILITADO), PIN 1 , PIN 2, PIN 3 EN BAJO (FULLSTEPPING)

		if(!signodeltaDEC){
			GPIOPinWrite(GPIO_PORTE_BASE,GPIO_PIN_2, 0 );//SENTIDO POSITIVO
		} else {
			GPIOPinWrite(GPIO_PORTE_BASE,GPIO_PIN_2, GPIO_PIN_2 );//SENTIDO NEGATIVO
		}
		TimerEnable(TIMER1_BASE, TIMER_A);// Habilito la operacion del timer: comienza la cuenta
	}
}

/**
 *		void TrackRA(float velocidad, int8_t sentido)
 *
 *
 *		la velocidad esta en unidades de velocidad siderea
 *	 	1 dia sidereo=86164,09164 seg
 *	 	1 GIRO COMPLETO=360°=1296000 ARCOSEGUNDOS.
 *	 	1 VELOCIDAD SIDEREA= 1296000 ARCOSEGUNDOS/86164,09164 seg = 15.03583338 ARCOSEGUNDOS/SEG.
 *
 *
 *		EN NUESTRO SISTEMA MECANICO:
 *		(1/16)*(6480 arcosegundos/paso completo)*(1/3)*(1/144)= (15/16)arcosegundos/microstep = 0.9375arcosegundos/microstep
 *
 *		microsteps por segundo necesarios:
 *
 *		(15.03583338 ARCOSEGUNDOS/SEG)/(0.9375arcosegundos/microstep)= 16.03822227 microsteps/segundo
 *
 *		CONCLUSION: 1 VELOCIDAD SIDEREA CORRESPONDE A ENVIAR AL DRIVER UNA SEÑAL DEL 16.03822227 Hz,
 *		EN MODO MICROSTEPPING(16).
 *
 *		CON UNA SEÑAL DE CLOCK DEL TIMER DE 40MHz, para lograr un semiperiodo:
 *		cuenta= (40000000/16.03822227)/2 = 1247021.002, debo cargar un entero: 1247021.
 *
 */
void TrackRA(float velocidad, int8_t sentido){

	float vel=velocidad;
	int8_t sent=sentido;

	uint32_t cuenta= (uint32_t)1247021/vel;

	TimerLoadSet(TIMER0_BASE, TIMER_A,cuenta - 1); //cargo el valor del semiperiodo en el timer

	IntEnable(INT_TIMER0A);// Se habilitan las interrupciones por timer0A

	TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);//Las interrupciones del timer0A se daran cuanto se alcanza la cuenta maxima,

	GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_2, 0); //ARRANCO EN CERO LA SEÑAL DE CLOCK

	GPIOPinWrite(GPIO_PORTC_BASE, GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7, 224 );
	//PIN 4 EN BAJO(ENABLE HABILITADO), PIN 5 , PIN 6, PIN 7 EN ALTO (MICROSTEPPING X 16)

	if(sent==0){
	GPIOPinWrite(GPIO_PORTA_BASE,GPIO_PIN_3, 0 );//SENTIDO POSITIVO
	}
	else{
	GPIOPinWrite(GPIO_PORTA_BASE,GPIO_PIN_3, 8 );//SENTIDO NEGATIVO
	}

	TimerEnable(TIMER0_BASE, TIMER_A);// Habilito la operacion del timer: comienza la cuenta

}


void TrackDEC(float velocidad, int8_t sentido){

		float vel=velocidad;
		int8_t sent=sentido;

		uint32_t cuenta0= (uint32_t)1247021/vel;

		TimerLoadSet(TIMER1_BASE, TIMER_A,cuenta0 - 1); //cargo el valor del semiperiodo en el timer

		IntEnable(INT_TIMER1A);// Se habilitan las interrupciones por timer0A

		TimerIntEnable(TIMER1_BASE, TIMER_TIMA_TIMEOUT);//Las interrupciones del timer0A se daran cuanto se alcanza la cuenta maxima,

		GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1, 0); //ARRANCO EN CERO LA SEÑAL DE CLOCK

		GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3,14 );
		//PIN 0 EN BAJO(ENABLE HABILITADO), PIN 1 , PIN 2, PIN 3 EN ALTO (MICROSTEPPING X 16)

		if(sent==0){
		GPIOPinWrite(GPIO_PORTE_BASE,GPIO_PIN_2, 0 );//SENTIDO POSITIVO
		}
		else{
		GPIOPinWrite(GPIO_PORTE_BASE,GPIO_PIN_2, 4 );//SENTIDO NEGATIVO
		}

		TimerEnable(TIMER1_BASE, TIMER_A);// Habilito la operacion del timer: comienza la cuenta

}

void StopTrackRA(void){
	TimerDisable(TIMER0_BASE, TIMER_A);// Paro el timer0.
	IntDisable(INT_TIMER0A);
	TimerIntDisable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
}

void StopTrackDEC(void){
	TimerDisable(TIMER1_BASE, TIMER_A);// Paro el timer0.
	IntDisable(INT_TIMER1A);
	TimerIntDisable(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
}





void Timer0IntHandler(void) //Servicio de interrupcion del timer0 RA
{
	TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT); // Clear the timer interrupt

	if(GPIOPinRead(GPIO_PORTE_BASE, GPIO_PIN_1))// Read the current state of the GPIO pin and write back the opposite state

		{
			GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1, 0); //Si la salida estaba en 1, pongo 0
			GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1, 0); //Si la salida estaba en 1, pongo 0
		}
		else
		{
			GPIOPinWrite(GPIO_PORTE_BASE,  GPIO_PIN_1,2);  //Si la salida estaba en 0, pongo 1
			GPIOPinWrite(GPIO_PORTF_BASE,  GPIO_PIN_1,2);  //Si la salida estaba en 0, pongo 1
			if(cuentafinalRA){
				cuentaactualRA++;
			} else {
				RA += 3107;//VA ACTUALIZANDO LA ASCENCION RECTA. 0XFFFFFF00 CORRESPONDE A 360 GRADOS= 1296000 ARCSEG.
			}
		}

}



void Timer1IntHandler(void) //Servicio de interrupcion del timer1
{
	TimerIntClear(TIMER1_BASE, TIMER_TIMA_TIMEOUT); // Clear the timer interrupt

		if(GPIOPinRead(GPIO_PORTA_BASE, GPIO_PIN_2))// Read the current state of the GPIO pin and write back the opposite state

			{
				GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_2, 0); //Si la salida estaba en 1, pongo 0
				GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2, 0); //Si la salida estaba en 1, pongo 0
			}
			else
			{
				GPIOPinWrite(GPIO_PORTA_BASE,  GPIO_PIN_2,4);  //Si la salida estaba en 0, pongo 1
				GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2, 4); //Si la salida estaba en 1, pongo 0
				if(cuentafinalDEC){
					cuentaactualDEC++;
				} else {
					DEC += 3107;//VA ACTUALIZANDO AZIMUTH. 0XFFFFFF00 CORRESPONDE A 360 GRADOS= 1296000 ARCSEG.
				}
			}

}



