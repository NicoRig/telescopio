/*
 * parser.h
 *
 *  Created on: Jun 20, 2016
 *      Author: nicolas
 */
#ifndef PARSER_H_
#define PARSER_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


extern int quecomandoes(char * cadena);
extern const char *comando[];
extern size_t cantcomandos;

#endif /* PARSER_H_ */
